import { useEffect, useState } from 'react'

interface Language { 
    id: number
    language: string 
}

function App() {
    const [languages, setLanguages] = useState<Array<Language>>([])

    useEffect(() => {
        initialize()
    }, [])

    const initialize = async () => {
        const response = await fetch('/languages')
        const languages = await response.json()
        setLanguages(languages)
    }

    return (
        <div className='App'>
            {languages.map(({id, language}) => <div key={id}>{language}</div>)}
        </div>
    )
}

export default App
