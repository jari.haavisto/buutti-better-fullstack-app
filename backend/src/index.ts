import express, { Request, Response } from 'express'
import { getLanguages } from './db'

const server = express()
server.use('/', express.static('./dist/client'))

server.get('/languages', async (_req: Request, res: Response) => {
    res.send(await getLanguages())
})

server.get('/version', (_req: Request, res: Response) => {
    res.send('2.0')
})

const { PORT } = process.env

server.listen(PORT, () => {
    console.log('Server listening @', PORT)
})