create table languages (id SERIAL primary key, language VARCHAR(32));

insert into languages (language) values
('Javascript'),('Python'),('Go'),('Java'),
('Kotlin'),('Scala'),('C'),('Swift'),
('TypeScript'),('Ruby');